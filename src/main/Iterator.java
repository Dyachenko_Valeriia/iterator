package main;

public interface Iterator {
    boolean hasNext();

    Book getNext();

    void reset();
}
