package main;

import java.util.List;

public class Library implements Iterator {
    private List<Book> books;
    private int currentPosition = 0;

    public Library(List<Book> books) {
        this.books = books;
    }

    @Override
    public boolean hasNext() {
        return books.size() > currentPosition;
    }

    @Override
    public Book getNext() {
        if (!hasNext()) {
            return null;
        }
        Book book = books.get(currentPosition);
        if (book == null) {
            System.out.println("Такую книгу уже взяли");
            return null;
        }
        currentPosition++;
        return book;
    }

    @Override
    public void reset() {
        currentPosition = 0;
    }
}
