package test;

import main.Book;
import main.Library;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;

public class TestLibrary {
    @Test
    public void testLibrary() {
        Book book = new Book("Преступление и наказание", 120, "Ф.М.Достоевский");
        Book book1 = new Book("Вишневый сад", 60, "А.П.Чехов");
        Book book2 = new Book("Горе от ума", 70, "А.С.Грибоедов");
        Book book3 = new Book("Герой нашего времени", 200, "М.Ю.Лермонтов");
        Book book4 = new Book("Евгений Онегин", 100, "А.С.Пушкин");
        List<Book> bookList = new ArrayList<>();
        Collections.addAll(bookList, book, book1, book2, book3, book4);
        Library library = new Library(bookList);
        library.getNext();
        library.getNext();
        assertTrue(library.hasNext());
    }

    @Test
    public void testLibrary1() {
        Book book = new Book("Преступление и наказание", 120, "Ф.М.Достоевский");
        Book book1 = new Book("Вишневый сад", 60, "А.П.Чехов");
        List<Book> bookList = new ArrayList<>();
        Collections.addAll(bookList, book, book1);
        Library library = new Library(bookList);
        library.getNext();
        library.getNext();
        assertFalse(library.hasNext());
    }

    @Test
    public void testLibrary2() {
        Book book = new Book("Преступление и наказание", 120, "Ф.М.Достоевский");
        Book book1 = new Book("Вишневый сад", 60, "А.П.Чехов");
        List<Book> bookList = new ArrayList<>();
        Collections.addAll(bookList, book, book1);
        Library library = new Library(bookList);
        library.getNext();
        library.getNext();
        assertNull(library.getNext());
    }

    @Test
    public void testLibrary3() {
        Book book = new Book("Преступление и наказание", 120, "Ф.М.Достоевский");
        Book book1 = new Book("Вишневый сад", 60, "А.П.Чехов");
        Book book2 = new Book("Горе от ума", 70, "А.С.Грибоедов");
        Book book3 = new Book("Герой нашего времени", 200, "М.Ю.Лермонтов");
        List<Book> bookList = new ArrayList<>();
        Collections.addAll(bookList, book, book1, book2, book3);
        Library library = new Library(bookList);
        library.getNext();
        library.getNext();
        assertEquals(book2, library.getNext());
    }

    @Test
    public void testLibrary4() {
        Book book = new Book("Преступление и наказание", 120, "Ф.М.Достоевский");
        Book book1 = new Book("Вишневый сад", 60, "А.П.Чехов");
        List<Book> bookList = new ArrayList<>();
        Collections.addAll(bookList, book, book1, null);
        Library library = new Library(bookList);
        library.getNext();
        library.getNext();
        assertNull(library.getNext());
    }
}
